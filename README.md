# Convection Suite

The Convection Suite of Tools

This is the metapackage that installs all components of Convection.

 - Convection IaC
   - [Core](https://gitlab.com/accidentallythecable-public/convection-suite/convection-iac)
   - [Accessor Plugins](https://gitlab.com/accidentallythecable-public/convection-suite/plugins/accessors)
   - [Actions Plugins](https://gitlab.com/accidentallythecable-public/convection-suite/plugins/actions)
   - [Connector Plugins](https://gitlab.com/accidentallythecable-public/convection-suite/plugins/connectors)
 - Convection Secrets Manager
   - [Server](https://gitlab.com/accidentallythecable-public/convection-suite/convection-secrets-manager)
   - [Client](https://gitlab.com/accidentallythecable-public/convection-suite/convection-secrets-client)
   - [Plugins](https://gitlab.com/accidentallythecable-public/convection-suite/plugins/secrets/)
